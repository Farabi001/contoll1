import fetch from "node-fetch";
import {promises as fs} from 'fs';

const main = async () => {
    const html = await getData("https://ru.wikipedia.org/wiki/TypeScript");
    await fs.writeFile('./dist/2.txt', html);
};

const getData = async (url: string) => {
    try {
        const response = await fetch(url);
        return await response.text();
    } catch (error) {
        console.error(error);
    }
};

main();
