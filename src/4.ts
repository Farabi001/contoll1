import fetch from "node-fetch";

const main = async () => {
    const html = await getData("https://ru.wikipedia.org/wiki/TypeScript");
    if (!html) {
        console.log('html is undefined');
        return;
    }

    const res = processData(html);
    console.log(res);
};

const getData = async (url: string) => {
    try {
        const response = await fetch(url);
        return await response.text();
    } catch (error) {
        console.error(error);
    }
};

const processData = (data: string) => {
    const dataWithoutScripts = deleteScripts(data);

    const dataWithoutHtmlTags = dataWithoutScripts.replace(/<[^>]+>/g,'');
    const dataWithoutLinebreaks = dataWithoutHtmlTags.replace(/[\r\n]+/gm, '');
    const dataWithoutMultipleSpaces = dataWithoutLinebreaks.replace(/  +/g, ' ');
    const dataWithoutTabs = dataWithoutMultipleSpaces.replace(/\t+/g, ' ');

    const words = dataWithoutTabs.split(' ');
    return words.reduce((count, word) => word.length % 2 === 0 ? count + 1 : count, 0)
};

const deleteScripts = (s: string) => {
    var SCRIPT_REGEX = /<script\b[^<]*(?:(?!<\/script>)<[^<]*)*<\/script>/gi;
    while (SCRIPT_REGEX.test(s)) {
        s = s.replace(SCRIPT_REGEX, "");
    }
    return s;
};

main();
