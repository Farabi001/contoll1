import {promises as fs} from 'fs';

const main = async () => {
    const data = await fs.readFile('./dist/2.txt', 'utf-8');
    const res = processData(data);

    console.log(res);
};

const processData = (data: string) => {
    const dataWithoutScripts = deleteScripts(data);

    const dataWithoutHtmlTags = dataWithoutScripts.replace(/<[^>]+>/g,'');
    const dataWithoutLinebreaks = dataWithoutHtmlTags.replace(/[\r\n]+/gm, '');
    const dataWithoutMultipleSpaces = dataWithoutLinebreaks.replace(/  +/g, ' ');
    const dataWithoutTabs = dataWithoutMultipleSpaces.replace(/\t+/g, ' ');

    const words = dataWithoutTabs.split(' ');
    return words.reduce((count, word) => word.length % 2 === 0 ? count + 1 : count, 0)
};

const deleteScripts = (s: string) => {
    var SCRIPT_REGEX = /<script\b[^<]*(?:(?!<\/script>)<[^<]*)*<\/script>/gi;
    while (SCRIPT_REGEX.test(s)) {
        s = s.replace(SCRIPT_REGEX, "");
    }
    return s;
};

main();
